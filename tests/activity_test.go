package tests

import (
	"../activities"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

var jogging = activities.GetStateById("0")
var biking = activities.GetStateById("1")
var walking = activities.GetStateById("2")
var hiking = activities.GetStateById("3")
var unknown = activities.GetStateById("4")

func TestString (t *testing.T){
	cases := []struct{
		in activities.STATE
		expected string
	}{
		{jogging, "Jogging"},
		{biking, "Biking"},
		{walking, "Walking"},
		{hiking, "Hiking"},
		{unknown, "Unknown Activity"},
	}

	for _, x := range cases{
		actual := x.in.String()
		assert.Equal(t, x.expected, actual)
	}
}

func TestIsPlausibleAvgSpeed(t *testing.T){
	cases := []struct{
		activity activities.STATE
		in float64
		expected bool
	}{
		{jogging, 3, true},
		{jogging, 0, false},
		{jogging, -3, false},
		{jogging, 20, false},
		{biking, 3, false},
		{biking, 5, true},
		{biking, 15, true},
		{walking, 3, false},
		{walking, 1, true},
		{walking, 1.2342352, true},
		{hiking, 3, false},
		{hiking, 1, true},
		{hiking, 1.5, true},
		{unknown, 1, false},
		{unknown, 10230123, false},
		{unknown, 15, false},
	}

	for _, x := range cases{
		actual := x.activity.IsPlausibleAvgSpeed(x.in)
		assert.Equal(t, x.expected, actual)
	}
}

func TestGetStandingFactor (t *testing.T){
	cases := []struct{
		in activities.STATE
		expected time.Duration
	}{
		{jogging, 6},
		{biking, 3},
		{walking, 10},
		{hiking, 12},
		{unknown, 0},
	}

	for _, x := range cases{
		actual := x.in.GetStandingFactor()
		assert.Equal(t, x.expected, actual)
	}
}

func TestGetStateById (t *testing.T){
	cases := []struct{
		in string
		expected activities.STATE
	}{
		{"0", jogging},
		{"1", biking},
		{"2", walking},
		{"3", hiking},
		{"4", unknown},
	}

	for _, x := range cases{
		actual := activities.GetStateById(x.in)
		assert.Equal(t, x.expected, actual)
	}
}

func TestGetId (t *testing.T){
	cases := []struct{
		in activities.STATE
		expected int
	}{
		{jogging, 0},
		{biking, 1},
		{walking, 2},
		{hiking, 3},
		{unknown, 0},
	}

	for _, x := range cases{
		actual := x.in.GetId()
		assert.Equal(t, x.expected, actual)
	}
}


