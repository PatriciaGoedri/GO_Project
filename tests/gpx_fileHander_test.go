package tests

import (
	"../gpx"
	"github.com/stretchr/testify/assert"
	"os"
	"path"
	"testing"
)

func TestUnzip(t *testing.T){
	dirName := path.Join("testfiles", "gpx", "testtodelete.txt")
	d, err := os.Create(dirName)
	if err != nil{
		return
	}
	d.Close()
	var cases = []struct{
		in string
		expectedEmpty bool
	}{
		{"testfiles/test_has_gpx.zip", false},
		{"testfiles/test_no_gpx.zip", true},
	}

	for _, x := range cases {
		actual, error := gpx.Unzip(x.in, "testfiles")
		//Cleanup
		if len(actual) > 0{
			for _, y := range actual{
				gpx.DeleteFile(y)
			}
		}
		assert.Equal(t, nil, error)
		assert.Equal(t, x.expectedEmpty, len(actual) == 0)
	}
}

func TestTryGetPathSplit(t *testing.T){
	var cases = []struct{
		in, expected string
	}{
		{"asdasdas/asdasdasda/asdasdas/asdasda", "/"},
		{"asdasdas/aasdasdas\\asdas//qwsad\\asdasd/", "\\"},
		{"asfasfa\\asfas/asdasf//\\asfasfas//", "\\"},
		{"\\\\\\", "\\"},
		{"",""},
		{"asdasdasdasdasdasdasdas", ""},
	}

	for _, x := range cases{
		actual := gpx.TryGetPathSplit(x.in)
		assert.Equal(t, x.expected, actual)
	}
}

func TestGetUserIdByFileName(t *testing.T){
	var cases = []struct{
		in, expected string
	}{
		{"test\\file.txt", "test"},
		{"test/file.txt", "test"},
		{"file.txt",""},
		{"test\\gpx\\file.txt","test"},
		{"test/gox/file.txt", "test"},
	}

	for _, x := range cases{
		actual := gpx.GetUserIdByFileName(x.in)
		assert.Equal(t, x.expected, actual)
	}
}

func TestDelete(t *testing.T){
	fileName := path.Join("testfiles", "gpx", "testtodelete.txt")
	//Create file to delete it
	f, err := os.Create(fileName)
	if err != nil{
		return
	}
	f.Close()

	var cases = []struct{
		in string
		expected bool
	}{
		{fileName, true},
		{"asdasdqwedqwdasdqwedqwdas.txt", false},
	}

	for _, x := range cases{
		actual := gpx.DeleteFile(x.in)
		assert.Equal(t, x.expected, actual)
	}
}