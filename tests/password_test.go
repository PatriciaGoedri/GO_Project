package tests

import (
	l "../login"
	"bytes"
	"testing"
)

//testing if salt is created
func TestSalt(t *testing.T) {
	l.Salt()
	return
}

//testing if each salt is 38 digits long
func TestSaltLenght(t *testing.T) {
	for i := 1; i <= 10; i++ {
		x := l.Salt()
		if len(x) == 38 {
			return
		}
	}
}

//testing if each salt is different
func TestSaltDifference(t *testing.T) {

}

//testing different hashes for same password with individual salt
func TestHashingDifference(t *testing.T) {
	salt1 := l.Salt()
	salt2 := l.Salt()

	if salt1 != salt2 {
		newSalt1 := salt1
		newSalt2 := salt2

		hash1 := l.Hash(newSalt1, "GoStrava")
		hash2 := l.Hash(newSalt2, "GoStrava")
		if bytes.Equal(hash1, hash2) {
			t.Errorf("Failed: Same hash for individual password!")
		}
	} else {
		t.Errorf("Failed: Same value for salt!")
	}
}

//Same Hash for equal password with same salting conditions
func TestHashingEquality(t *testing.T) {
	salt := l.Salt()

	hash1 := l.Hash(salt, "GoStrava")
	hash2 := l.Hash(salt, "GoStrava")

	if !bytes.Equal(hash1, hash2) {
		t.Errorf("Failed: Different hash for same conditions")
	}
}

//Different Hash for individiual password including equal salt
func TestHashingRandom(t *testing.T) {
	salt := l.Salt()

	hash1 := l.Hash(salt, "StravaGo")
	hash2 := l.Hash(salt, "GoStrava")
	if bytes.Equal(hash1, hash2) {
		t.Errorf("Same hash individual password")
	}
}
