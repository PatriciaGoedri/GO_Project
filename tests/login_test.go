package tests

import (
	l "../login"
	"bytes"
	"fmt"
	"testing"
	"time"
)

type Credentials struct {
	Username string `json:"user"`
	Password []byte `json:"password"`
	//Id       *big.Int `json:"id"`
	Created time.Time
	Salt    string `json:"salt"`
}

func TestCheckPassword(t *testing.T) {
	username := "test"
	salt := l.Salt()
	password := "123456765432"
	encryptedPassword := l.Hash(salt, password)

	var creds = []Credentials{
		{Username: "test", Password: encryptedPassword, Created: time.Now(), Salt: salt}}

	for i, _ := range creds {
		if creds[i].Username == username {

			fmt.Println("User found")
			return
		} else {
			t.Errorf("Failed: User doesn't exist")
		}

	}
}

func TestCheckUser(t *testing.T) {

	username := "test"
	salt := l.Salt()
	password := "123456765432"
	encryptedPassword := l.Hash(salt, password)

	var creds = []Credentials{
		{Username: username, Password: encryptedPassword, Created: time.Now(), Salt: salt}}

	for i, _ := range creds {
		if bytes.Compare(creds[i].Password, l.Hash(creds[i].Salt, password)) == 0 {
			fmt.Println("Password correct")

		} else {
			t.Errorf("Failed: Password isn't correct!")
		}
		return

	}

}
