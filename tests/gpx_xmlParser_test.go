package tests

import (
	"../gpx"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestParseGpx(t *testing.T) {
	var case0 = `<?xml version="1.0" encoding="UTF-8"?><gpx version="1.1" creator="Urban Biker" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd http://www.garmin.com/xmlschemas/GpxExtensions/v3 http://www.garmin.com/xmlschemas/GpxExtensionsv3.xsd http://www.garmin.com/xmlschemas/TrackPointExtension/v2 http://www.garmin.com/xmlschemas/TrackPointExtensionv2.xsd" xmlns="http://www.topografix.com/GPX/1/1" xmlns:gpxtpx="http://www.garmin.com/xmlschemas/TrackPointExtension/v2" xmlns:gpxpx="http://www.garmin.com/xmlschemas/PowerExtensionv1.xsd" xmlns:gpxx="http://www.garmin.com/xmlschemas/GpxExtensions/v3" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><metadata><link href="http://www.urban-bike-computer.com/"><text>Urban Biker</text></link><time>2019-09-14T13:14:17.094Z</time></metadata><trk><name>Cube – Sa., 14. Sep. 2019, 15:14</name><trkseg></trkseg><trkseg><trkpt lat="49.3549890600" lon="9.1519649400"><ele>172.50</ele><time>2019-09-14T13:14:30.276Z</time><extensions><gpxtpx:TrackPointExtension><gpxtpx:atemp>22.8</gpxtpx:atemp><gpxtpx:speed>5.54</gpxtpx:speed></gpxtpx:TrackPointExtension></extensions></trkpt><trkpt lat="49.3550175500" lon="9.1520317100"><ele>172.23</ele><time>2019-09-14T13:14:32.291Z</time><extensions><gpxtpx:TrackPointExtension><gpxtpx:atemp>22.8</gpxtpx:atemp><gpxtpx:speed>4.86</gpxtpx:speed></gpxtpx:TrackPointExtension></extensions></trkpt><trkpt lat="49.3550991300" lon="9.1520247100"><ele>171.90</ele><time>2019-09-14T13:14:34.290Z</time><extensions><gpxtpx:TrackPointExtension><gpxtpx:atemp>22.8</gpxtpx:atemp><gpxtpx:speed>4.54</gpxtpx:speed></gpxtpx:TrackPointExtension></extensions></trkpt></trkseg></trk></gpx>`
	var case1 = `<?xml version="1.0" encoding="UTF-8"?><gpx version="1.1" creator="Urban Biker" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd http://www.garmin.com/xmlschemas/GpxExtensions/v3 http://www.garmin.com/xmlschemas/GpxExtensionsv3.xsd http://www.garmin.com/xmlschemas/TrackPointExtension/v2 http://www.garmin.com/xmlschemas/TrackPointExtensionv2.xsd" xmlns="http://www.topografix.com/GPX/1/1" xmlns:gpxtpx="http://www.garmin.com/xmlschemas/TrackPointExtension/v2" xmlns:gpxpx="http://www.garmin.com/xmlschemas/PowerExtensionv1.xsd" xmlns:gpxx="http://www.garmin.com/xmlschemas/GpxExtensions/v3" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><trk><name>Cube – So., 22. Sep. 2019, 14:34</name><trkseg></trkseg><trkseg><trkpt lat="49.3547878700" lon="9.1510938600"><ele>168.54</ele><time>2019-09-22T12:34:18.148Z</time><extensions><gpxtpx:TrackPointExtension><gpxtpx:atemp>20.5</gpxtpx:atemp></gpxtpx:TrackPointExtension></extensions></trkpt><trkpt lat="49.3548811800" lon="9.1511142300"><ele>168.54</ele><time>2019-09-22T12:34:20.148Z</time><extensions><gpxtpx:TrackPointExtension><gpxtpx:atemp>20.7</gpxtpx:atemp><gpxtpx:speed>5.35</gpxtpx:speed></gpxtpx:TrackPointExtension></extensions></trkpt><trkpt lat="49.3550104900" lon="9.1510435100"><ele>168.09</ele><time>2019-09-22T12:34:22.149Z</time><extensions><gpxtpx:TrackPointExtension><gpxtpx:atemp>20.8</gpxtpx:atemp><gpxtpx:speed>5.77</gpxtpx:speed></gpxtpx:TrackPointExtension></extensions></trkpt></trkseg></trk></gpx>`
  	var expected0 = gpx.Gpx{
		Version: `1.1`,
		Creator: `Urban Biker`,
		Metadata: gpx.Metadata{
			Link: gpx.Link{
				Href: `http://www.urban-bike-computer.com/`,
				Text: `Urban Biker`,
			},
			Time: time.Date(2019, 9, 14, 13, 14, 17, 94000000, time.UTC),
		},
		Tracking: gpx.Trk{
			Name: `Cube – Sa., 14. Sep. 2019, 15:14`,
			Trkseg: []gpx.Trkseg{
				{},
				{Trkpt: []gpx.Trkpt{
					{
						Lat: 49.3549890600,
						Lon: 9.1519649400,
						Ele: 172.50,
						Time: time.Date(2019, 9, 14, 13, 14, 30, 276000000, time.UTC),
					},
					{
						Lat: 49.3550175500,
						Lon: 9.1520317100,
						Ele: 172.23,
						Time: time.Date(2019, 9, 14, 13, 14, 32, 291000000, time.UTC),
					},
					{
						Lat: 49.3550991300,
						Lon: 9.1520247100,
						Ele: 171.90,
						Time: time.Date(2019, 9, 14, 13, 14, 34, 290000000, time.UTC),
					},
				}},
			},
		},
	}
	var expected1 = gpx.Gpx {
		Version: `1.1`,
		Creator: `Urban Biker`,
		Tracking: gpx.Trk{
			Name: `Cube – So., 22. Sep. 2019, 14:34`,
			Trkseg: []gpx.Trkseg{
				{},
				{Trkpt: []gpx.Trkpt{
					{
						Lat: 49.3547878700,
						Lon: 9.1510938600,
						Ele: 168.54,
						Time: time.Date(2019, 9, 22, 12, 34, 18, 148000000, time.UTC),
					},
					{
						Lat: 49.3548811800,
						Lon: 9.1511142300,
						Ele: 168.54,
						Time: time.Date(2019, 9, 22, 12, 34, 20, 148000000, time.UTC),
					},
					{
						Lat: 49.3550104900,
						Lon: 9.1510435100,
						Ele: 168.09,
						Time: time.Date(2019, 9, 22, 12, 34, 22, 149000000, time.UTC),
					},
				}},
			},
		},
	}

	var cases = []struct {
		in       string
		expected gpx.Gpx
	}{
		{case0, expected0},
		{case1, expected1},
	}

	for _, x := range cases {
		actual := gpx.ParseGpx([]byte(x.in))
		assert.Equal(t, x.expected, actual)
	}
}