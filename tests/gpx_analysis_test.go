package tests

import (
	"../gpx"
	"container/list"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

var testPoint0 = gpx.Trkpt{
Lat:     49.3530908500,
Lon:     9.1494986700,
Ele:     178.36,
Time:    time.Date(2019, 9, 21, 13, 54, 33, 679000000, time.UTC),
}

var testPoint1 = gpx.Trkpt{
Lat:     49.3529767500,
Lon:     9.1494703900,
Ele:     178.07,
Time:    time.Date(2019, 9, 21, 13, 54, 35, 684000000, time.UTC),
}

var testPoint2 = gpx.Trkpt{
	Lat:     49.3530908500,
	Lon:     9.1494986700,
	Ele:     178.36,
	Time:    time.Date(2019, 9, 21, 13, 54, 35, 684000000, time.UTC),
}

var testPoint3 = gpx.Trkpt{
	Lat:     49.3529767500,
	Lon:     9.1494703900,
	Ele:     178.07,
	Time:    time.Date(2019, 9, 21, 13, 54, 33, 679000000, time.UTC),
}

var testPoints3 = []gpx.Trkpt{
	{Lat: 49.3530908500, Lon: 9.1494986700, Ele: 178.36, Time: time.Date(2019, 9, 21, 13, 54, 33, 679000000, time.UTC)},
	{Lat: 49.3529767500, Lon: 9.1494703900, Ele: 178.07, Time: time.Date(2019, 9, 21, 13, 54, 35, 684000000, time.UTC)},
	{Lat: 49.3528805400, Lon: 9.1494763500, Ele: 177.63, Time: time.Date(2019, 9, 21, 13, 54, 37, 684000000, time.UTC)},
}

var testPointsFakeStands = []gpx.Trkpt{
	{Lat: 49.3499425900, Lon: 9.1454789800, Ele: 178.18, Time: time.Date(2019, 9, 21, 13, 55, 59, 707000000, time.UTC)},
	{Lat: 49.3499017300, Lon: 9.1452966600, Ele: 178.04, Time: time.Date(2019, 9, 21, 13, 56, 1, 707000000, time.UTC)},
	{Lat: 49.3498486700, Lon: 9.1451112700, Ele: 177.92, Time: time.Date(2019, 9, 21, 13, 56, 3, 710000000, time.UTC)},
	{Lat: 49.3497958800, Lon: 9.1449303300, Ele: 177.84, Time: time.Date(2019, 9, 21, 13, 56, 5, 710000000, time.UTC)},
	{Lat: 49.3497350700, Lon: 9.1447422500, Ele: 177.78, Time: time.Date(2019, 9, 21, 13, 56, 7, 710000000, time.UTC)},
	//Stand1
	{Lat: 49.3497350700, Lon: 9.1447442300, Ele: 177.78, Time: time.Date(2019, 9, 21, 13, 56, 9, 710000000, time.UTC)},
	{Lat: 49.3497350700, Lon: 9.1447452500, Ele: 177.78, Time: time.Date(2019, 9, 21, 13, 56, 11, 710000000, time.UTC)},
	{Lat: 49.3497350700, Lon: 9.1447622500, Ele: 177.78, Time: time.Date(2019, 9, 21, 13, 56, 13, 710000000, time.UTC)},
	{Lat: 49.3497350700, Lon: 9.1447732500, Ele: 177.78, Time: time.Date(2019, 9, 21, 13, 56, 15, 710000000, time.UTC)},
	{Lat: 49.3497350700, Lon: 9.1447612500, Ele: 177.78, Time: time.Date(2019, 9, 21, 13, 56, 21, 710000000, time.UTC)},
	//EndStand1
	{Lat: 49.3496857800, Lon: 9.1445633200, Ele: 177.74, Time: time.Date(2019, 9, 21, 13, 56, 23, 707000000, time.UTC)},
	{Lat: 49.3496335500, Lon: 9.1443704900, Ele: 177.70, Time: time.Date(2019, 9, 21, 13, 56, 25, 707000000, time.UTC)},
	{Lat: 49.3495662000, Lon: 9.1441991000, Ele: 177.72, Time: time.Date(2019, 9, 21, 13, 56, 29, 708000000, time.UTC)},
	{Lat: 49.3495162600, Lon: 9.1440196600, Ele: 177.82, Time: time.Date(2019, 9, 21, 13, 56, 32, 708000000, time.UTC)},
	{Lat: 49.3493181200, Lon: 9.1434958000, Ele: 177.94, Time: time.Date(2019, 9, 21, 13, 56, 34, 712000000, time.UTC)},
	//ShortStand (NotDetected)
	{Lat: 49.3493181200, Lon: 9.1435058000, Ele: 177.94, Time: time.Date(2019, 9, 21, 13, 56, 36, 712000000, time.UTC)},
	//EndShortStand
	{Lat: 49.3492698300, Lon: 9.1433293400, Ele: 178.00, Time: time.Date(2019, 9, 21, 13, 56, 38, 698000000, time.UTC)},
	{Lat: 49.3492093900, Lon: 9.1431611000, Ele: 178.07, Time: time.Date(2019, 9, 21, 13, 56, 40, 706000000, time.UTC)},
	{Lat: 49.3490509900, Lon: 9.1427781500, Ele: 178.16, Time: time.Date(2019, 9, 21, 13, 56, 42, 701000000, time.UTC)},
	{Lat: 49.3489491400, Lon: 9.1425448900, Ele: 178.19, Time: time.Date(2019, 9, 21, 13, 56, 44, 689000000, time.UTC)},
	{Lat: 49.3488657800, Lon: 9.1423845800, Ele: 178.19, Time: time.Date(2019, 9, 21, 13, 56, 46, 689000000, time.UTC)},
	//Stand2
	{Lat: 49.3488657800, Lon: 9.1424845800, Ele: 178.19, Time: time.Date(2019, 9, 21, 13, 56, 48, 689000000, time.UTC)},
	{Lat: 49.3488657800, Lon: 9.1424865800, Ele: 178.19, Time: time.Date(2019, 9, 21, 13, 56, 50, 689000000, time.UTC)},
	{Lat: 49.3488657800, Lon: 9.1424847800, Ele: 178.19, Time: time.Date(2019, 9, 21, 13, 56, 52, 689000000, time.UTC)},
	//EndStand2
	{Lat: 49.3487740900, Lon: 9.1421997200, Ele: 178.17, Time: time.Date(2019, 9, 21, 13, 56, 54, 695000000, time.UTC)},
	{Lat: 49.3486931400, Lon: 9.1420372300, Ele: 178.14, Time: time.Date(2019, 9, 21, 13, 56, 56, 684000000, time.UTC)},
}

var testPoints100 = []gpx.Trkpt{
	{Lat: 49.3530908500, Lon: 9.1494986700, Ele: 178.36, Time: time.Date(2019, 9, 21, 13, 54, 33, 679000000, time.UTC)},
	{Lat: 49.3529767500, Lon: 9.1494703900, Ele: 178.07, Time: time.Date(2019, 9, 21, 13, 54, 35, 684000000, time.UTC)},
	{Lat: 49.3528805400, Lon: 9.1494763500, Ele: 177.63, Time: time.Date(2019, 9, 21, 13, 54, 37, 684000000, time.UTC)},
	{Lat: 49.3527807400, Lon: 9.1494778000, Ele: 177.39, Time: time.Date(2019, 9, 21, 13, 54, 39, 692000000, time.UTC)},
	{Lat: 49.3526887500, Lon: 9.1494452600, Ele: 177.36, Time: time.Date(2019, 9, 21, 13, 54, 41, 686000000, time.UTC)},
	{Lat: 49.3526396500, Lon: 9.1493415500, Ele: 177.55, Time: time.Date(2019, 9, 21, 13, 54, 43, 685000000, time.UTC)},
	{Lat: 49.3525858900, Lon: 9.1488532500, Ele: 177.88, Time: time.Date(2019, 9, 21, 13, 54, 51, 677000000, time.UTC)},
	{Lat: 49.3525444000, Lon: 9.1487797800, Ele: 177.98, Time: time.Date(2019, 9, 21, 13, 54, 53, 693000000, time.UTC)},
	{Lat: 49.3524369200, Lon: 9.1487487900, Ele: 178.06, Time: time.Date(2019, 9, 21, 13, 54, 57, 688000000, time.UTC)},
	{Lat: 49.3523629900, Lon: 9.1487781000, Ele: 178.15, Time: time.Date(2019, 9, 21, 13, 54, 59, 685000000, time.UTC)},
	{Lat: 49.3522772300, Lon: 9.1488136700, Ele: 178.24, Time: time.Date(2019, 9, 21, 13, 55, 1, 693000000, time.UTC)},
	{Lat: 49.3521945500, Lon: 9.1488359800, Ele: 178.30, Time: time.Date(2019, 9, 21, 13, 55, 3, 695000000, time.UTC)},
	{Lat: 49.3521133100, Lon: 9.1488569500, Ele: 178.37, Time: time.Date(2019, 9, 21, 13, 55, 5, 689000000, time.UTC)},
	{Lat: 49.3520189900, Lon: 9.1488635700, Ele: 178.46, Time: time.Date(2019, 9, 21, 13, 55, 7, 691000000, time.UTC)},
	{Lat: 49.3519206000, Lon: 9.1488372600, Ele: 178.55, Time: time.Date(2019, 9, 21, 13, 55, 9, 689000000, time.UTC)},
	{Lat: 49.3518225300, Lon: 9.1487002000, Ele: 178.63, Time: time.Date(2019, 9, 21, 13, 55, 11, 693000000, time.UTC)},
	{Lat: 49.3517151400, Lon: 9.1485946300, Ele: 178.66, Time: time.Date(2019, 9, 21, 13, 55, 13, 686000000, time.UTC)},
	{Lat: 49.3515949200, Lon: 9.1485051200, Ele: 178.65, Time: time.Date(2019, 9, 21, 13, 55, 15, 687000000, time.UTC)},
	{Lat: 49.3514816200, Lon: 9.1484167400, Ele: 178.59, Time: time.Date(2019, 9, 21, 13, 55, 17, 682000000, time.UTC)},
	{Lat: 49.3513570900, Lon: 9.1482750200, Ele: 178.48, Time: time.Date(2019, 9, 21, 13, 55, 19, 684000000, time.UTC)},
	{Lat: 49.3512505400, Lon: 9.1482411200, Ele: 178.37, Time: time.Date(2019, 9, 21, 13, 55, 21, 686000000, time.UTC)},
	{Lat: 49.3511118000, Lon: 9.1481551200, Ele: 178.34, Time: time.Date(2019, 9, 21, 13, 55, 23, 712000000, time.UTC)},
	{Lat: 49.3510133700, Lon: 9.1480758400, Ele: 178.32, Time: time.Date(2019, 9, 21, 13, 55, 25, 712000000, time.UTC)},
	{Lat: 49.3509005600, Lon: 9.1480404000, Ele: 178.28, Time: time.Date(2019, 9, 21, 13, 55, 27, 708000000, time.UTC)},
	{Lat: 49.3507877900, Lon: 9.1479921500, Ele: 178.36, Time: time.Date(2019, 9, 21, 13, 55, 29, 704000000, time.UTC)},
	{Lat: 49.3506952200, Lon: 9.1479419500, Ele: 178.42, Time: time.Date(2019, 9, 21, 13, 55, 31, 718000000, time.UTC)},
	{Lat: 49.3506252000, Lon: 9.1478143400, Ele: 178.49, Time: time.Date(2019, 9, 21, 13, 55, 33, 713000000, time.UTC)},
	{Lat: 49.3505499500, Lon: 9.1476581000, Ele: 178.58, Time: time.Date(2019, 9, 21, 13, 55, 35, 715000000, time.UTC)},
	{Lat: 49.3505107800, Lon: 9.1474748600, Ele: 178.67, Time: time.Date(2019, 9, 21, 13, 55, 37, 712000000, time.UTC)},
	{Lat: 49.3504752800, Lon: 9.1473127100, Ele: 178.75, Time: time.Date(2019, 9, 21, 13, 55, 39, 705000000, time.UTC)},
	{Lat: 49.3504178500, Lon: 9.1471642200, Ele: 178.82, Time: time.Date(2019, 9, 21, 13, 55, 41, 696000000, time.UTC)},
	{Lat: 49.3503448800, Lon: 9.1469775600, Ele: 178.88, Time: time.Date(2019, 9, 21, 13, 55, 43, 705000000, time.UTC)},
	{Lat: 49.3502843600, Lon: 9.1468056100, Ele: 178.88, Time: time.Date(2019, 9, 21, 13, 55, 45, 706000000, time.UTC)},
	{Lat: 49.3502212100, Lon: 9.1466362600, Ele: 178.90, Time: time.Date(2019, 9, 21, 13, 55, 47, 707000000, time.UTC)},
	{Lat: 49.3501592000, Lon: 9.1464549300, Ele: 178.83, Time: time.Date(2019, 9, 21, 13, 55, 49, 712000000, time.UTC)},
	{Lat: 49.3501145400, Lon: 9.1462714900, Ele: 178.76, Time: time.Date(2019, 9, 21, 13, 55, 51, 702000000, time.UTC)},
	{Lat: 49.3500701200, Lon: 9.1460721800, Ele: 178.62, Time: time.Date(2019, 9, 21, 13, 55, 53, 708000000, time.UTC)},
	{Lat: 49.3500160100, Lon: 9.1458855300, Ele: 178.49, Time: time.Date(2019, 9, 21, 13, 55, 55, 710000000, time.UTC)},
	{Lat: 49.3499866200, Lon: 9.1456826400, Ele: 178.34, Time: time.Date(2019, 9, 21, 13, 55, 57, 708000000, time.UTC)},
	{Lat: 49.3499425900, Lon: 9.1454789800, Ele: 178.18, Time: time.Date(2019, 9, 21, 13, 55, 59, 707000000, time.UTC)},
	{Lat: 49.3499017300, Lon: 9.1452966600, Ele: 178.04, Time: time.Date(2019, 9, 21, 13, 56, 1, 707000000, time.UTC)},
	{Lat: 49.3498486700, Lon: 9.1451112700, Ele: 177.92, Time: time.Date(2019, 9, 21, 13, 56, 3, 710000000, time.UTC)},
	{Lat: 49.3497958800, Lon: 9.1449303300, Ele: 177.84, Time: time.Date(2019, 9, 21, 13, 56, 5, 710000000, time.UTC)},
	{Lat: 49.3497350700, Lon: 9.1447422500, Ele: 177.78, Time: time.Date(2019, 9, 21, 13, 56, 7, 710000000, time.UTC)},
	{Lat: 49.3496857800, Lon: 9.1445633200, Ele: 177.74, Time: time.Date(2019, 9, 21, 13, 56, 9, 707000000, time.UTC)},
	{Lat: 49.3496335500, Lon: 9.1443704900, Ele: 177.70, Time: time.Date(2019, 9, 21, 13, 56, 11, 707000000, time.UTC)},
	{Lat: 49.3495662000, Lon: 9.1441991000, Ele: 177.72, Time: time.Date(2019, 9, 21, 13, 56, 13, 708000000, time.UTC)},
	{Lat: 49.3495162600, Lon: 9.1440196600, Ele: 177.82, Time: time.Date(2019, 9, 21, 13, 56, 15, 708000000, time.UTC)},
	{Lat: 49.3493181200, Lon: 9.1434958000, Ele: 177.94, Time: time.Date(2019, 9, 21, 13, 56, 21, 712000000, time.UTC)},
	{Lat: 49.3492698300, Lon: 9.1433293400, Ele: 178.00, Time: time.Date(2019, 9, 21, 13, 56, 23, 698000000, time.UTC)},
	{Lat: 49.3492093900, Lon: 9.1431611000, Ele: 178.07, Time: time.Date(2019, 9, 21, 13, 56, 25, 706000000, time.UTC)},
	{Lat: 49.3490509900, Lon: 9.1427781500, Ele: 178.16, Time: time.Date(2019, 9, 21, 13, 56, 29, 701000000, time.UTC)},
	{Lat: 49.3489491400, Lon: 9.1425448900, Ele: 178.19, Time: time.Date(2019, 9, 21, 13, 56, 32, 689000000, time.UTC)},
	{Lat: 49.3488657800, Lon: 9.1423845800, Ele: 178.19, Time: time.Date(2019, 9, 21, 13, 56, 34, 689000000, time.UTC)},
	{Lat: 49.3487740900, Lon: 9.1421997200, Ele: 178.17, Time: time.Date(2019, 9, 21, 13, 56, 36, 695000000, time.UTC)},
	{Lat: 49.3486931400, Lon: 9.1420372300, Ele: 178.14, Time: time.Date(2019, 9, 21, 13, 56, 38, 684000000, time.UTC)},
	{Lat: 49.3485902700, Lon: 9.1418436000, Ele: 178.10, Time: time.Date(2019, 9, 21, 13, 56, 40, 685000000, time.UTC)},
	{Lat: 49.3485052300, Lon: 9.1416578300, Ele: 178.01, Time: time.Date(2019, 9, 21, 13, 56, 43, 000000000, time.UTC)},
	{Lat: 49.3484258900, Lon: 9.1414657200, Ele: 177.91, Time: time.Date(2019, 9, 21, 13, 56, 44, 998000000, time.UTC)},
	{Lat: 49.3483321300, Lon: 9.1412648200, Ele: 177.79, Time: time.Date(2019, 9, 21, 13, 56, 46, 991000000, time.UTC)},
	{Lat: 49.3482610000, Lon: 9.1410692200, Ele: 177.64, Time: time.Date(2019, 9, 21, 13, 56, 48, 990000000, time.UTC)},
	{Lat: 49.3481747100, Lon: 9.1408405800, Ele: 177.49, Time: time.Date(2019, 9, 21, 13, 56, 51, 2000000, time.UTC)},
	{Lat: 49.3480658300, Lon: 9.1406900600, Ele: 177.33, Time: time.Date(2019, 9, 21, 13, 56, 53, 000000000, time.UTC)},
	{Lat: 49.3479790700, Lon: 9.1404757200, Ele: 177.15, Time: time.Date(2019, 9, 21, 13, 56, 55, 22000000, time.UTC)},
	{Lat: 49.3478771400, Lon: 9.1402769400, Ele: 176.98, Time: time.Date(2019, 9, 21, 13, 56, 56, 990000000, time.UTC)},
	{Lat: 49.3478022800, Lon: 9.1400780700, Ele: 176.81, Time: time.Date(2019, 9, 21, 13, 56, 59, 18000000, time.UTC)},
	{Lat: 49.3477080400, Lon: 9.1398990800, Ele: 176.65, Time: time.Date(2019, 9, 21, 13, 57, 1, 25000000, time.UTC)},
	{Lat: 49.3476050200, Lon: 9.1397077800, Ele: 176.48, Time: time.Date(2019, 9, 21, 13, 57, 3, 000000000, time.UTC)},
	{Lat: 49.3475168400, Lon: 9.1394831000, Ele: 176.31, Time: time.Date(2019, 9, 21, 13, 57, 4, 998000000, time.UTC)},
	{Lat: 49.3474120700, Lon: 9.1392909400, Ele: 176.15, Time: time.Date(2019, 9, 21, 13, 57, 7, 4000000, time.UTC)},
	{Lat: 49.3473409300, Lon: 9.1390870400, Ele: 175.99, Time: time.Date(2019, 9, 21, 13, 57, 9, 2000000, time.UTC)},
	{Lat: 49.3472488700, Lon: 9.1388966000, Ele: 175.83, Time: time.Date(2019, 9, 21, 13, 57, 11, 1000000, time.UTC)},
	{Lat: 49.3471454500, Lon: 9.1387228500, Ele: 175.64, Time: time.Date(2019, 9, 21, 13, 57, 12, 985000000, time.UTC)},
	{Lat: 49.3470256900, Lon: 9.1385380900, Ele: 175.42, Time: time.Date(2019, 9, 21, 13, 57, 15, 14000000, time.UTC)},
	{Lat: 49.3469385600, Lon: 9.1383365700, Ele: 175.20, Time: time.Date(2019, 9, 21, 13, 57, 17, 000000000, time.UTC)},
	{Lat: 49.3468361400, Lon: 9.1381345600, Ele: 174.94, Time: time.Date(2019, 9, 21, 13, 57, 18, 993000000, time.UTC)},
	{Lat: 49.3467547400, Lon: 9.1379045900, Ele: 174.65, Time: time.Date(2019, 9, 21, 13, 57, 21, 000000000, time.UTC)},
	{Lat: 49.3466690100, Lon: 9.1376719700, Ele: 174.33, Time: time.Date(2019, 9, 21, 13, 57, 23, 5000000, time.UTC)},
	{Lat: 49.3465682900, Lon: 9.1374568600, Ele: 174.00, Time: time.Date(2019, 9, 21, 13, 57, 24, 998000000, time.UTC)},
	{Lat: 49.3464639800, Lon: 9.1372456200, Ele: 173.65, Time: time.Date(2019, 9, 21, 13, 57, 26, 994000000, time.UTC)},
	{Lat: 49.3463614800, Lon: 9.1370564700, Ele: 173.33, Time: time.Date(2019, 9, 21, 13, 57, 29, 5000000, time.UTC)},
	{Lat: 49.3462917100, Lon: 9.1368701300, Ele: 173.05, Time: time.Date(2019, 9, 21, 13, 57, 31, 4000000, time.UTC)},
	{Lat: 49.3461946800, Lon: 9.1366821000, Ele: 172.83, Time: time.Date(2019, 9, 21, 13, 57, 33, 7000000, time.UTC)},
	{Lat: 49.3460999300, Lon: 9.1365222400, Ele: 172.63, Time: time.Date(2019, 9, 21, 13, 57, 35, 4000000, time.UTC)},
	{Lat: 49.3459451100, Lon: 9.1361889200, Ele: 172.59, Time: time.Date(2019, 9, 21, 13, 57, 38, 688000000, time.UTC)},
	{Lat: 49.3458666900, Lon: 9.1360781900, Ele: 172.62, Time: time.Date(2019, 9, 21, 13, 57, 40, 701000000, time.UTC)},
	{Lat: 49.3457787600, Lon: 9.1359480900, Ele: 172.69, Time: time.Date(2019, 9, 21, 13, 57, 42, 694000000, time.UTC)},
	{Lat: 49.3456910700, Lon: 9.1357936700, Ele: 172.79, Time: time.Date(2019, 9, 21, 13, 57, 44, 685000000, time.UTC)},
	{Lat: 49.3456043500, Lon: 9.1356593800, Ele: 172.90, Time: time.Date(2019, 9, 21, 13, 57, 46, 684000000, time.UTC)},
	{Lat: 49.3455232700, Lon: 9.1355295700, Ele: 172.99, Time: time.Date(2019, 9, 21, 13, 57, 48, 687000000, time.UTC)},
	{Lat: 49.3454138600, Lon: 9.1354059600, Ele: 173.12, Time: time.Date(2019, 9, 21, 13, 57, 51, 8000000, time.UTC)},
	{Lat: 49.3453120400, Lon: 9.1352607400, Ele: 173.23, Time: time.Date(2019, 9, 21, 13, 57, 53, 13000000, time.UTC)},
	{Lat: 49.3452295500, Lon: 9.1351707200, Ele: 173.30, Time: time.Date(2019, 9, 21, 13, 57, 54, 997000000, time.UTC)},
	{Lat: 49.3452057400, Lon: 9.1350666000, Ele: 173.47, Time: time.Date(2019, 9, 21, 13, 57, 57, 8000000, time.UTC)},
	{Lat: 49.3451951300, Lon: 9.1349780200, Ele: 173.70, Time: time.Date(2019, 9, 21, 13, 58, 00, 994000000, time.UTC)},
	{Lat: 49.3451092100, Lon: 9.1348898400, Ele: 173.75, Time: time.Date(2019, 9, 21, 13, 58, 9, 1000000, time.UTC)},
	{Lat: 49.3450295400, Lon: 9.1348188200, Ele: 173.79, Time: time.Date(2019, 9, 21, 13, 58, 11, 7000000, time.UTC)},
	{Lat: 49.3449625800, Lon: 9.1347537600, Ele: 173.82, Time: time.Date(2019, 9, 21, 13, 58, 12, 997000000, time.UTC)},
	{Lat: 49.3448934100, Lon: 9.1346713200, Ele: 173.87, Time: time.Date(2019, 9, 21, 13, 58, 15, 1000000, time.UTC)},
	{Lat: 49.3448298500, Lon: 9.1345574300, Ele: 173.88, Time: time.Date(2019, 9, 21, 13, 58, 17, 2000000, time.UTC)},
	{Lat: 49.3447973500, Lon: 9.1344591300, Ele: 173.86, Time: time.Date(2019, 9, 21, 13, 58, 18, 999000000, time.UTC)},
}

var EmptyArray []gpx.Trkpt

var case0 = []gpx.Trkpt{testPoint0, testPoint0}
var case1 = []gpx.Trkpt{testPoint0, testPoint1}
var case2 = []gpx.Trkpt{testPoint0, testPoint2}
var case3 = []gpx.Trkpt{testPoint0, testPoint3}

var gpxObject0 = gpx.Gpx{
	Version: `1.1`,
	Creator: `Urban Biker`,
	Metadata: gpx.Metadata{
		Link: gpx.Link{
			Href: `http://www.urban-bike-computer.com/`,
			Text: `Urban Biker`,
		},
		Time: time.Date(2019, 9, 21, 13, 54, 16, 15400000, time.UTC),
	},
	Tracking: gpx.Trk{
		Name: `Cube – Sa., 21. Sep. 2019, 15:54`,
		Trkseg: []gpx.Trkseg{
			{},
			{Trkpt: testPoints3},
		},
	},
}

var activity0 = gpx.AnalysedActivity{
	Start:                    time.Date(2019, 9, 21, 13, 54, 33, 679000000, time.UTC),
	End:                      time.Date(2019, 9, 21, 13, 54, 37, 684000000, time.UTC),
	FullDistance:             23.55840335398888,
	MaxSpeed:                 6.409792005271972,
	AvgSpeed:                 5.8822480284616425,
	AvgSpeedWithoutStanding:  5.8822480284616425,
	Duration:                 4.005,
	DurationStandings:        0,
	DurationWithoutStandings: 4.005,
	Standings:                list.New(),
	Activity:				  biking,
	PlausibleActivity:		  true,
}

var gpxObject1 = gpx.Gpx{
	Version: `1.1`,
	Creator: `Urban Biker`,
	Metadata: gpx.Metadata{
		Link: gpx.Link{
			Href: `http://www.urban-bike-computer.com/`,
			Text: `Urban Biker`,
		},
		Time: time.Date(2019, 9, 21, 13, 54, 16, 15400000, time.UTC),
	},
	Tracking: gpx.Trk{
		Name: `Cube – Sa., 21. Sep. 2019, 15:54`,
		Trkseg: []gpx.Trkseg{
			{},
			{Trkpt: testPointsFakeStands},
		},
	},
}

var activity1 = gpx.AnalysedActivity{
	Start:                    time.Date(2019, 9, 21, 13, 55, 59, 707000000, time.UTC),
	End:                      time.Date(2019, 9, 21, 13, 56, 56, 684000000, time.UTC),
	FullDistance:             306.2398078435078,
	MaxSpeed:                 21.895848494605005,
	AvgSpeed:                 5.374796985511835,
	AvgSpeedWithoutStanding:  7.791731061314287,
	Duration:                 56.977,
	DurationStandings:        20,
	DurationWithoutStandings: 36.977,
	Standings:                StandList(),
	Activity:				  biking,
	PlausibleActivity:		  true,
}

var gpxObject2 = gpx.Gpx{
	Version: `1.1`,
	Creator: `Urban Biker`,
	Metadata: gpx.Metadata{
		Link: gpx.Link{
			Href: `http://www.urban-bike-computer.com/`,
			Text: `Urban Biker`,
		},
		Time: time.Date(2019, 9, 21, 13, 54, 16, 15400000, time.UTC),
	},
	Tracking: gpx.Trk{
		Name: `Cube – Sa., 21. Sep. 2019, 15:54`,
		Trkseg: []gpx.Trkseg{
			{},
		},
	},
}

var activity2 = gpx.AnalysedActivity{
	Start:                    time.Date(2019, 9, 21, 13, 54, 16, 15400000, time.UTC),
	End:                      time.Date(2019, 9, 21, 13, 54, 16, 15400000, time.UTC),
	FullDistance:             0,
	MaxSpeed:                 0,
	AvgSpeed:                 0,
	AvgSpeedWithoutStanding:  0,
	Duration:                 0,
	DurationStandings:        0,
	DurationWithoutStandings: 0,
	Standings:                nil,
	Activity:				  biking,
}

func TestToRadians(t *testing.T){
	var cases = []struct {
		in, expected float64
	}{
		{0, 0},
		{10, 0.17453292519943295},
		{20, 0.3490658503988659},
		{30, 0.5235987755982988},
		{40, 0.6981317007977318},
		{50, 0.8726646259971648},
		{60, 1.0471975511965976},
		{70, 1.2217304763960306},
		{80, 1.3962634015954636},
		{90, 1.5707963267948966},
	}

	for _, x := range cases {
		actual := gpx.ToRadians(x.in)
		assert.Equal(t, x.expected, actual)
	}
}

func TestCalculateDistanceBetweenTwoGpsSpots(t *testing.T){
	var cases = []struct {
		lat0, lon0, lat1, lon1, expected float64
	}{
		{0,0,0,0,0},
		{49.352780,9.145524,52.523403,13.411400, 462053.6884705274},
		{49.352780,9.145524, 49.341730,9.104660, 3205.0809025608005},
		{49.3530908500,9.1494986700,49.3529767500,9.1494703900, 12.851632970570302},
		{49.3530908500,9.1494986700,49.3530908500,9.1494986700, 0},
	}

	for _, x := range cases {
		actual := gpx.CalculateDistanceBetweenTwoGpsSpots(x.lat0, x.lon0, x.lat1, x.lon1)
		assert.Equal(t, x.expected, actual)
	}
}

func TestCalculateFullDistanceOfMultipleGpxPoints(t *testing.T){
	var cases = []struct{
		in []gpx.Trkpt
		expected float64
	}{
		{case0, 0},
		{case1, 12.851632970570302},
		{case2, 0},
		{case3, 12.851632970570302},
		{testPoints3, 23.55840335398888},
	}

	for _, x := range cases{
		actual := gpx.CalculateFullDistanceOfMultipleGpxPoints(x.in)
		assert.Equal(t, x.expected, actual)
	}
}

func StandList() *list.List{
	standList := list.New()
	standList.PushBack(gpx.Standing{
		Start: time.Date(2019, 9, 21, 13, 56, 7, 710000000, time.UTC),
		End: time.Date(2019, 9, 21, 13, 56, 21, 710000000, time.UTC),
	})
	standList.PushBack(gpx.Standing{
		Start: time.Date(2019, 9, 21, 13, 56, 46, 689000000, time.UTC),
		End: time.Date(2019, 9, 21, 13, 56, 52, 689000000, time.UTC),
	})
	return standList
}

func TestFindStandings(t *testing.T){
	var cases = []struct{
		in       []gpx.Trkpt
		expected *list.List
	}{
		{testPoints3, list.New()},
		{testPointsFakeStands, StandList()},
	}

	for _, x := range cases	{
		actual := gpx.FindStandings(x.in, biking)
		assert.Equal(t, x.expected, actual)
	}
}

func TestCalculateDurationOfAllGpxPoints(t *testing.T){
	var cases = []struct{
		in []gpx.Trkpt
		expected float64
	}{
		{case0, 0},
		{case1, 2.005},
		{case2, 2.005},
		{case3, 0},
		{testPointsFakeStands, 56.977},
		{testPoints3, 4.005},
	}

	for _, x := range cases {
		actual := gpx.CalculateDurationOfAllGpxPoints(x.in)
		assert.Equal(t, x.expected, actual)
	}
}

func TestCalculateSpeedBetweenTwoGpxPoints(t *testing.T){
	var cases = []struct {
		in0, in1 gpx.Trkpt
		expected float64
	}{
		{testPoint0, testPoint0, 0},
		{testPoint0, testPoint1, 6.409792005271972},
		{testPoint0, testPoint2, 0},
		{testPoint0, testPoint3, 0},
	}

	for _, x := range cases {
		actual := gpx.CalculateSpeedBetweenTwoGpxPoints(x.in0, x.in1)
		assert.Equal(t, x.expected, actual)
	}
}

func TestCalculateAvgSpeedBetweenAllGpxPoints(t *testing.T){
	var cases = []struct{
		in []gpx.Trkpt
		expected float64
	}{
		{case0, 0},
		{case1, 6.409792005271972},
		{case2, 0},
		{case3, 0},
		{testPointsFakeStands, 5.374796985511835},
		{testPoints3, 5.8822480284616425},
	}

	for _, x := range cases {
		actual := gpx.CalculateAvgSpeedBetweenAllGpxPoints(x.in)
		assert.Equal(t, x.expected, actual)
	}
}

func TestCalculateDurationOfStandings(t *testing.T){
	standings0 := gpx.FindStandings(testPointsFakeStands, biking)
	standings1 := gpx.FindStandings(testPoints3, biking)

	var cases = []struct{
		in *list.List
		expected float64
	}{
		{standings0, 20},
		{standings1, 0},
	}

	for _, x := range cases {
		actual := gpx.CalculateDurationOfStandings(x.in)
		assert.Equal(t, x.expected, actual)
	}

}

func TestCalculateAvgSpeedWithoutStandings(t *testing.T){
	standings0 := gpx.FindStandings(testPointsFakeStands, biking)
	standings1 := gpx.FindStandings(testPoints3, biking)

	var cases = []struct{
		in0      []gpx.Trkpt
		in1      *list.List
		expected float64
	}{
		{testPointsFakeStands, standings0, 7.791731061314287},
		{testPoints3, standings1,5.8822480284616425},
	}

	for _, x := range cases {
		actual := gpx.CalculateAvgSpeedWithoutStandings(x.in0, x.in1)
		assert.Equal(t, x.expected, actual)
	}
}

func TestCalculateTopSpeed(t *testing.T){
	var cases = []struct{
		in []gpx.Trkpt
		expected float64
	}{
		{case0, 0},
		{case1, 6.409792005271972},
		{case2, 0},
		{case3, 0},
		{testPointsFakeStands, 21.895848494605005},
		{testPoints3, 6.409792005271972},
	}

	for _, x := range cases {
		actual := gpx.CalculateTopSpeed(x.in)
		assert.Equal(t, x.expected, actual)
	}
}

func TestAnalyseGpxObject(t *testing.T){
	var cases = []struct{
		in gpx.Gpx
		expected gpx.AnalysedActivity
	}{
		{gpxObject0, activity0},
		{gpxObject1, activity1},
		{gpxObject2, activity2},
	}

	for _, x := range cases{
		actual := gpx.AnalyseGpxObject(x.in, "", "1", "", "")
		assert.Equal(t, x.expected, actual)
	}
}