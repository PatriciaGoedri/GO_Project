/*
created: 1100592

*/
package login

import (
	"math/big"
	"time"
)

//contains the format for the json file with all relevant information
type Credentials struct {
	Username string   `json:"user"`
	Password []byte   `json:"password"`
	Id       *big.Int `json:"id"`
	Created  time.Time
	Salt     string `json:"salt"`
}
