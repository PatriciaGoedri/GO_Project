/*
created: 1100592
*/
package login

import (
	"crypto/rand"
	"crypto/sha256"
	"fmt"
	"math/big"
)

/*
	Salting given password.
	Must be at least 32 random bytes because of sha256
	ASCII : 1 digit equals 7 bit --> 32 Byte == 256 * 7 Bit = 1792

	go returns only 38 digits

*/

func Salt() string {

	rNumber, _ := rand.Int(rand.Reader, big.NewInt(9223372036854775807))
	lenghtNumber := rNumber.String()
	lenght := 1792

	for len(lenghtNumber) < lenght {
		newNumber, _ := rand.Int(rand.Reader, big.NewInt(9223372036854775807))
		lenghtNewNumber := newNumber.String()

		lenghtNumber := lenghtNumber + lenghtNewNumber

		fmt.Println(lenghtNumber)
		return lenghtNumber
	}

	return lenghtNumber
	//fmt.Println(lenghtNumber)
}

/*
	Hashing salted password
	references [1, page 83]  in documentation
*/
func Hash(salt string, password string) []byte {
	x := sha256.New()
	x.Write([]byte(salt + password))
	return x.Sum(nil)
}

func getID() *big.Int {
	id, err := rand.Int(rand.Reader, big.NewInt(100000))

	if err != nil {
		return nil
	}

	return id

}
