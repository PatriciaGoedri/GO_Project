/*
created: 1100592
*/
package login

import (
	"encoding/json"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"path"
	"strings"
	"time"
)

/*Register new user due registration request*/
func RegisterUser(w http.ResponseWriter, r *http.Request) {
	//Parse data from html template for processing
	fmt.Println("method:", r.Method)
	if r.Method == "GET" {
		//t, _ := template.ParseFiles("template/registry.html")
		t := template.Must(template.ParseFiles(path.Join("template", "registry.html")))
		t.Execute(w, nil)
	} else {
		r.ParseForm()
		//create variables for processing with new data
		username := strings.Join(r.Form["username"], "")
		password := strings.Join(r.Form["password"], "")
		password2 := strings.Join(r.Form["confirmPassword"], "")

		//check if fields are filled
		usernameT := available(username)
		passwordT := available(password)
		password2T := available(password2)
		//check if user already exists
		userexist, _, _ := checkUser(username)

		if userexist == true {
			fmt.Fprint(w, "User already existing. For new registry: refresh the page.")
			return
		}
		if len(password) < 12 {
			fmt.Fprint(w, "Required password is too short! For new registry: refresh the page.")
			return
		}

		if usernameT || passwordT || password2T {
			fmt.Fprint(w, "Data missing. For new registry: refresh the page.")
			return
		}
		//If passwords are equal then add User to JSON file
		if password == password2 {

			err := addUser(username, password)
			if err != nil {
				http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			}
			t := template.Must(template.ParseFiles(path.Join("template", "login.html")))
			//t, _ := template.ParseFiles("template/login.html")
			t.Execute(w, nil)
		} else {
			//t, _ := template.ParseFiles("template/registry.html")
			fmt.Println("Registry failed!")
			t := template.Must(template.ParseFiles(path.Join("template", "registry.html")))
			t.Execute(w, nil)
		}

	}
}

//Check if field is filled for processing
func available(data string) bool {
	if len(data) == 0 {
		return true
	} else {
		return false
	}
}

//add new User to jsonfile
func addUser(username string, password string) (err error) {

	jsonDatei, err := ioutil.ReadFile(path.Join("login", "credentials.json")) //read latest json file
	if err != nil {
		return
	}
	var users []Credentials

	//fill the required variables
	salt := Salt()
	encryptedPassword := Hash(salt, password)
	id := getID()

	if err := json.Unmarshal(jsonDatei, &users); err != nil {
		log.Println(err)
	}

	created := time.Now()
	//Add array of information to slice
	users = append(users, Credentials{Created: created, Username: username, Password: encryptedPassword, Salt: salt, Id: id}) //add new user to slice

	erg, err := json.MarshalIndent(users, "", "")
	if err != nil {
		log.Fatalf("Marshaling failed: %s", err)
	}
	ioutil.WriteFile(path.Join("login", "credentials.json"), erg, 0644) //renew file

	return
}
