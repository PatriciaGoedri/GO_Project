/*
created: 1100592
*/
package login

import (
	"net/http"
)

//implementing Basic Authentification
//inspired by lecture
func LoggingMiddleware(handler http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		username, password, ok := r.BasicAuth()
		checkU, _, _ := checkUser(username)
		checkP, _, _ := checkPassword(password)
		if ok && checkU && checkP {
			handler(w, r)
		} else {
			w.Header().Set("WWW-Authenticate", "Basic realm=\"Go-Strava!\"")
			http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
		}
	}
}

//inspired from [2, chapter 6.2]
func SecureHeaders(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("X-XSS-Protection", "1; mode=block")
		w.Header().Set("X-Frame-Options", "deny")

		next.ServeHTTP(w, r)
	})
}
