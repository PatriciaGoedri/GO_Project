/*
created: 1100592 ; 4688442
*/
package login

import (
	//"../gpx"
	"bytes"
	"encoding/json"
	"fmt"
	"path"

	//"html/template"
	"io/ioutil"
	//"strings"
)

/*
func LoginUser unnecessary due to BaicAuthentification

func LoginUser(w http.ResponseWriter, r *http.Request) {

	fmt.Println("method:", r.Method)
	if r.Method == "GET" {
		//t, _ := template.ParseFiles("template/login.html")
		t := template.Must(template.ParseFiles(path.Join("template", "login.html")))
		t.Execute(w, nil)
	} else {
		r.ParseForm()

		username := strings.Join(r.Form["username"], "")
		password := strings.Join(r.Form["password"], "")

		usernameT := available(username)
		passwordT := available(password)

		if usernameT || passwordT {
			http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		}

		result, err, user := checkUser(username)
		if err != nil {
			fmt.Println(err)
		}

		resultP, err, user := checkPassword(password)
		if err != nil {
			fmt.Println(err)
		}

		if result == true && resultP == true {
			gpx.IndexHandler(w, r, "")
		} else {
			http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		}

	}
}
*/

/*
Validates Username information;
Check if user is already registrated
*/

func checkUser(username string) (result bool, err error, credentials Credentials) {
	result = false
	jsonDatei, err := ioutil.ReadFile(path.Join("login", "credentials.json")) //reas newest json
	if err != nil {
		return
	}
	//get Array of existing Credentials
	users := []Credentials{}

	err = json.Unmarshal(jsonDatei, &users) //unmarshal json for processing
	if err != nil {
		return
	}
	//Check if given exists in jsonfile
	for i, _ := range users {
		if users[i].Username == username {
			credentials = users[i]
			fmt.Println("User found")
			result = true
			return
		}
	}
	return
}

/*
Validates Username information;
Check if user is already registrated
*/
func checkPassword(password string) (resultP bool, err error, credentials Credentials) {
	resultP = false
	jsonDatei, err := ioutil.ReadFile(path.Join("login", "credentials.json")) //read newest json
	if err != nil {
		return
	}
	//get Array of existing Credentials
	users := []Credentials{}

	err = json.Unmarshal(jsonDatei, &users) //unmarshaling for processing
	if err != nil {
		return
	}
	//check if given password existing in json. Remind that password is stored hashed.
	for i, _ := range users {
		if bytes.Compare(users[i].Password, Hash(users[i].Salt, password)) == 0 {
			credentials = users[i]
			fmt.Println("Password confirmed")
			resultP = true
			return
		}
	}
	return
}

func GetIdForUser(userName string) string {
	jsonFile, err := ioutil.ReadFile(path.Join("login", "credentials.json")) //Read newest file
	if err != nil {
		return ""
	}
	users := []Credentials{}

	err = json.Unmarshal(jsonFile, &users)
	if err != nil {
		return ""
	}
	for _, x := range users {
		if bytes.Compare([]byte(x.Username), []byte(userName)) == 0 {
			return x.Id.String()
		}
	}
	return ""
}
