/*
created: 4688442
*/
package gpx

import (
	"encoding/xml"
	"errors"
	"io/ioutil"
	"os"
)

//Parses the content of a file into a GPX object
func ConvertFileContent(filePath string) (Gpx, error) {
	xmlFile, err := os.Open(filePath)
	if err != nil {
		var gpx Gpx
		return gpx, errors.New(`Unable to read file ` + filePath)
	}
	data, _ := ioutil.ReadAll(xmlFile)
	xmlFile.Close()
	return ParseGpx(data), nil
}

//Parse the XML byte array into a GPX object
func ParseGpx(data []byte) Gpx {
	var gpx Gpx
	xml.Unmarshal(data, &gpx)
	return gpx
}
