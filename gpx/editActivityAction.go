/*
created: 4688442
*/
package gpx

import (
	"encoding/json"
	"fmt"
	"html/template"
	"io/ioutil"
	"net/http"
	"os"
	"path"
	"strconv"
)

//Handler to show the side to edit a activity
func EditActivity(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Show Edit Activity")
	r.ParseMultipartForm(1024 * 16)
	jsonFile := r.FormValue("fileName")
	done, activity := ReadJsonFile(w, jsonFile)
	if !done {
		fmt.Println(w, "Error reading activity")
	}
	data := struct {
		Activity, Comment, FileName, JsonFileName string
	}{
		strconv.Itoa(activity.Activity.GetId()),
		activity.Comment,
		activity.FileName,
		activity.JsonFileName,
	}

	t, _ := template.ParseFiles(path.Join(".", "template", "editactivity.html"))
	t.Execute(w, data)
}

//Handler to execute a edit of a activity
//The analysis of the file will be repeated to get new standings ect. for a maybe changed activity type
func ExecuteEditActivity(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Edit Activity")
	r.ParseMultipartForm(1024 * 16)
	file := r.FormValue("fileName")
	gpxContent, err := ConvertFileContent(file)
	if err != nil {
		fmt.Println(err)
		fmt.Println(w, "Error edit activity")
		return
	}
	comment := r.FormValue("comment")
	activityType := r.FormValue("activity")
	jsonFile := r.FormValue("jsonFileName")
	activity := AnalyseGpxObject(gpxContent, comment, activityType, file, jsonFile)
	json, err := json.Marshal(activity)
	if err != nil {
		fmt.Println(err)
		fmt.Println(w, "Error edit activity")
		return
	}
	err = ioutil.WriteFile(jsonFile, json, os.ModePerm)
	if err != nil {
		fmt.Println(err)
		fmt.Println(w, "Error edit activity")
		return
	}

	userId := GetUserIdByFileName(jsonFile)
	if userId != "" {
		IndexHandler(w, r, "")
	}
}
