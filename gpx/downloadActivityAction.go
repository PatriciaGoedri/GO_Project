/*
created: 4688442
*/
package gpx

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"strconv"
	"time"
)

//Handler to download a Gpx file
//Inspired by https://mrwaggel.be/post/golang-transmit-files-over-a-nethttp-server-to-clients/
func DownloadActivity(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Download Activity")
	r.ParseMultipartForm(1024 * 16)
	fileName := r.FormValue("fileName")
	fmt.Println("Download File: " + fileName)

	f, err := os.Open(fileName)
	defer f.Close()
	if err != nil {
		println(w, "Error download file")
		return
	}

	header := make([]byte, 512)
	f.Read(header)
	contentType := http.DetectContentType(header)

	stat, _ := f.Stat()
	size := strconv.FormatInt(stat.Size(), 10)

	downloadName := time.Now().Format("2006-01-02_15-04-05") + ".gpx"

	w.Header().Set("Content-Disposition", "attachment; filename="+downloadName)
	w.Header().Set("Content-Type", contentType)
	w.Header().Set("Content-Length", size)

	f.Seek(0, 0)
	io.Copy(w, f)
}
