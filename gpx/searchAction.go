/*
created: 4688442
*/
package gpx

import (
	"fmt"
	"net/http"
)

//Handler for the search function of the activities
func SearchHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Search in activities")
	r.ParseMultipartForm(1024 * 16)
	searchText := r.FormValue("searchText")
	IndexHandler(w, r, searchText)
}
