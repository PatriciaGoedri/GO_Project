/*
created: 4688442
*/
package gpx

import (
	"../login"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"path"
	"strings"
)

//Handler of the upload
func UploadGpx(w http.ResponseWriter, r *http.Request) {
	fmt.Println(w, "Uploading file")
	r.ParseMultipartForm(1024 * 16)
	userName, _, _ := r.BasicAuth()
	userId := login.GetIdForUser(userName)
	if userId == "" {
		fmt.Println(w, "Upload failed: no user")
		return
	}
	fileExtension, uploadFile, done := UploadFile(r, w, userId)
	if !done {
		return
	}
	fmt.Println(w, "Uploading succeeded")

	analysisItems, done := GetAnalysisFiles(fileExtension, uploadFile, w, userId)
	if !done {
		return
	}

	fmt.Println(w, "Start analyse file(s)")
	count := AnalyseFiles(analysisItems, w, r)
	fmt.Println(w, "Finished Analysis.")
	fmt.Println(w, "Successfully analysed files: "+string(count))
	IndexHandler(w, r, "")
}

//Analyses a bunch of uploaded files and returns, how many read in successfully
func AnalyseFiles(analysisItems []string, w http.ResponseWriter, r *http.Request) int {
	count := 0
	for _, f := range analysisItems {
		gpxContent, err := ConvertFileContent(f)
		if err != nil {
			fmt.Println(err)
			fmt.Println(w, "Error analyse one file")
			DeleteFile(f)
		}
		fileName := strings.Replace(f, ".gpx", ".json", 1)
		split := TryGetPathSplit(fileName)
		if split != "" {
			fileName = strings.Replace(fileName, split+"gpx", "", 1)
		}
		activity := AnalyseGpxObject(gpxContent, r.FormValue("comment"), r.FormValue("activity"), f, fileName)
		json, err := json.Marshal(activity)
		if err != nil {
			fmt.Println(err)
			fmt.Println(w, "Error save analysed file")
			DeleteFile(f)
		}
		err = ioutil.WriteFile(fileName, json, os.ModePerm)
		if err != nil {
			fmt.Println(err)
			fmt.Println(w, "Error save analysed file")
			DeleteFile(f)
		}
		count++
	}
	return count
}

//Gets the files to analyse
//Unzips the upload file if necessary
func GetAnalysisFiles(fileExtension string, uploadFile string, w http.ResponseWriter, userId string) ([]string, bool) {
	var analysisItems []string
	if fileExtension == ".gpx" {
		analysisItems = append(analysisItems, uploadFile)
		return analysisItems, true
	} else {
		fmt.Println(w, "Start unzip file")
		analysisItems, err := Unzip(uploadFile, userId)
		DeleteFile(uploadFile)
		if err != nil {
			fmt.Println(err)
			fmt.Println(w, "Error unzip File")
			return nil, false
		}
		if len(analysisItems) == 0 {
			fmt.Println(err)
			fmt.Println(w, "No gpx files in unzipped")
			return nil, false
		}
		fmt.Println(w, "Unzip succeeded")
		return analysisItems, true
	}
}

//"Copies" the file to the server
func UploadFile(r *http.Request, w http.ResponseWriter, userId string) (string, string, bool) {
	file, handler, err := r.FormFile("gpxFile")
	if err != nil {
		fmt.Println("Error retrieving file")
		fmt.Println(err)
		return "", "", false
	}
	defer file.Close()
	fmt.Printf("Uploaded File: %+v\n", handler.Filename)
	fileExtension := strings.ToLower(handler.Filename[len(handler.Filename)-4:])
	if fileExtension != ".gpx" && fileExtension != ".zip" {
		fmt.Println(w, "File extension not supported")
		return "", "", false
	}

	pattern := "upload-*" + fileExtension
	gpxFolder := path.Join(userId, "gpx")
	err = os.MkdirAll(gpxFolder, os.ModePerm)
	if err != nil {
		fmt.Println(w, "Error save file")
		fmt.Println("Error create user dir for user: " + userId)
		fmt.Println(err)
		return "", "", false
	}

	uploadFile, err := ioutil.TempFile(gpxFolder, pattern)
	if err != nil {
		fmt.Println(err)
		return "", "", false
	}
	defer uploadFile.Close()

	fileByte, err := ioutil.ReadAll(file)
	if err != nil {
		fmt.Println(err)
		return "", "", false
	}
	uploadFile.Write(fileByte)
	file.Close()
	name := uploadFile.Name()
	uploadFile.Close()
	return fileExtension, name, true
}
