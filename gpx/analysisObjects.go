/*
created: 4688442
*/
package gpx

import (
	"GO_Project/activities"
	"container/list"
	"time"
)

type Standing struct {
	Start, End time.Time
}

//Struct for the analysed activity
//All Analysis will be saved in this struct
//Not all are shown on web app and some are more exact than on the web app
type AnalysedActivity struct {
	Start                    time.Time        `json:"start"`
	End                      time.Time        `json:"end"`
	FullDistance             float64          `json:"fulldistance"`
	MaxSpeed                 float64          `json:"maxseed"`
	AvgSpeed                 float64          `json:"avgspeed"`
	AvgSpeedWithoutStanding  float64          `json:"avgspeedwithoutstanding"`
	Duration                 float64          `json:"duration"`
	DurationStandings        float64          `json:"durationstandings"`
	DurationWithoutStandings float64          `json:"durationwithoutstandings"`
	Standings                *list.List       `json:"standings"`
	Comment                  string           `json:"comment"`
	FileName                 string           `json:"filename"`
	JsonFileName             string           `json:"jsonfilename"`
	Activity                 activities.STATE `json:"activity"`
	PlausibleActivity        bool             `json:"plausibleactivity"`
}

//Struct for the web app output
//Everything will be parsed in strings and some are formatted
type OutputActivity struct {
	Activity, Start, End, Duration, DurationStandings, Distance, AvgSpeed, MaxSpeed, Comment, PlausibleActivity, FileName, JsonFileName string
}
