/*
created: 4688442
*/
package gpx

import (
	"encoding/xml"
	"time"
)

//Structs for the XML parse of the GPX Files
type Gpx struct {
	XmlName  xml.Name `xml:"gpx"`
	Version  string   `xml:"version,attr"`
	Creator  string   `xml:"creator,attr"`
	Metadata Metadata `xml:"metadata"`
	Tracking Trk      `xml:"trk"`
}

type Metadata struct {
	XmlName xml.Name  `xml:"metadata"`
	Link    Link      `xml:"link"`
	Time    time.Time `xml:"time"`
}

type Link struct {
	XmlName xml.Name `xml:"link"`
	Href    string   `xml:"href,attr"`
	Text    string   `xml:"text"`
}

type Trk struct {
	XmlName xml.Name `xml:"trk"`
	Name    string   `xml:"name"`
	Trkseg  []Trkseg `xml:"trkseg"`
}

type Trkseg struct {
	XmlName xml.Name `xml:"trkseg"`
	Trkpt   []Trkpt  `xml:"trkpt"`
}

type Trkpt struct {
	XmlName xml.Name  `xml:"trkpt"`
	Lat     float64   `xml:"lat,attr"`
	Lon     float64   `xml:"lon,attr"`
	Ele     float64   `xml:"ele"`
	Time    time.Time `xml:"time"`
}
