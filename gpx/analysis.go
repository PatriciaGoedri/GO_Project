/*
created: 4688442
*/
package gpx

import (
	"GO_Project/activities"
	"container/list"
	"math"
	"time"
)

const (
	radiusEarth = 6371000
)

//Uses the Haversine formula to calculate the great-distance between two points
//Input: lat0, lon0 GPS data of first Point; lat1, lon1 GPS data of second Point
//Output: Distance in meters
func CalculateDistanceBetweenTwoGpsSpots(lat0, lon0, lat1, lon1 float64) float64 {
	var phi0 = ToRadians(lat0)
	var phi1 = ToRadians(lat1)
	var deltaPhi = ToRadians(lat1 - lat0)
	var deltaLambda = ToRadians(lon1 - lon0)
	var a = math.Sin(deltaPhi/2.0)*math.Sin(deltaPhi/2.0) +
		math.Cos(phi0)*math.Cos(phi1)*
			math.Sin(deltaLambda/2.0)*math.Sin(deltaLambda/2.0)
	var c = 2 * math.Atan2(math.Sqrt(a), math.Sqrt(1-a))
	return radiusEarth * c
}

//Calculates the radians of a degree number
//Input: number as degree
//Output: number as radians
func ToRadians(degree float64) float64 {
	return degree * (math.Pi / 180)
}

//Sums the distance between multiple gps points
//Input: Gps Points
//Output: full distance between the points in meters
func CalculateFullDistanceOfMultipleGpxPoints(points []Trkpt) float64 {
	var distance float64 = 0
	var lon = math.NaN()
	var lat = math.NaN()
	for _, x := range points {
		if math.IsNaN(lon) || math.IsNaN(lat) {
			lon = x.Lon
			lat = x.Lat
		}
		distance = CalculateDistanceBetweenTwoGpsSpots(lat, lon, x.Lat, x.Lon) + distance
		lon = x.Lon
		lat = x.Lat
	}
	return distance
}

//Looking for standings in the trkpoints, standings are defined in a distance smaller than 10 meters in a duration of x seconds
//x is defined by the activities.STATE
//Input: Gps Points; Type of Activity
//Output: List of Stands, made of a start and end time of the standing
func FindStandings(points []Trkpt, activity activities.STATE) *list.List {
	var lon = points[0].Lon
	var lat = points[0].Lat
	var startStand = points[0].Time
	var endStand = points[0].Time
	stands := list.New()
	for _, x := range points {
		distance := CalculateDistanceBetweenTwoGpsSpots(lat, lon, x.Lat, x.Lon)
		if distance > 10 {
			if endStand.Sub(startStand) >= time.Second*activity.GetStandingFactor() {
				stands.PushBack(Standing{
					Start: startStand,
					End:   endStand,
				})
			}
			startStand = x.Time
			endStand = x.Time
			lon = x.Lon
			lat = x.Lat
		} else {
			endStand = x.Time
		}
	}
	return stands
}

//Calculates avg speed between two trkpoints [m/s]
//Input: Two Gps Points
//Output: Avg Speed between the two points in meters per second
func CalculateSpeedBetweenTwoGpxPoints(point0, point1 Trkpt) float64 {
	distance := CalculateDistanceBetweenTwoGpsSpots(point0.Lat, point0.Lon, point1.Lat, point1.Lon)
	duration := CalculateDurationOfAllGpxPoints([]Trkpt{point0, point1})
	if duration == 0 {
		return 0
	}
	return distance / duration
}

//Calculates average speed of the trkpoints [m/s]
//Input: Multiple Gps Points
//Output: Avg Speed of all Points in meters per second
func CalculateAvgSpeedBetweenAllGpxPoints(points []Trkpt) float64 {
	distance := CalculateFullDistanceOfMultipleGpxPoints(points)
	duration := CalculateDurationOfAllGpxPoints(points)
	if duration == 0 {
		return 0
	}
	return distance / duration
}

//Calculates average speed without the points that are declared as standings
//Input: Multiple Gps Points, list of standings (start and end time)
//Output: Avg Speed in meters per second
func CalculateAvgSpeedWithoutStandings(points []Trkpt, standings *list.List) float64 {
	distance := CalculateDistanceWithoutStandings(points, standings)
	durationOfStandings := CalculateDurationOfStandings(standings)
	duration := CalculateDurationOfAllGpxPoints(points) - durationOfStandings
	if duration <= 0 {
		return 0
	}
	return distance / duration
}

//Sums the distance between multiple gps points without the points that are declared as standings
//Input: Multiple Gps Points, list of standings (start and end time)
//Output: distance in meters
func CalculateDistanceWithoutStandings(points []Trkpt, standings *list.List) float64 {
	var calcPoints []Trkpt
	if standings.Len() == 0 {
		calcPoints = points
	} else {
		for _, x := range points {
			isStanding := false
			for y := standings.Front(); y != nil; y = y.Next() {
				if PointIsPartOfStanding(x, y.Value.(Standing)) {
					isStanding = true
					break
				}
				if x.Time.Before(y.Value.(Standing).Start) {
					break
				}
			}
			if !isStanding {
				calcPoints = append(calcPoints, x)
			}
		}
	}
	return CalculateFullDistanceOfMultipleGpxPoints(calcPoints)
}

//Checks if the point is part of the standing
//Input: One Gps Point, One Standing
func PointIsPartOfStanding(point Trkpt, standing Standing) bool {
	return (point.Time.After(standing.Start) && point.Time.Before(standing.End)) || point.Time.Equal(standing.Start) || point.Time.Equal(standing.End)
}

//Calculates the duration of all Gpx Points in seconds
func CalculateDurationOfAllGpxPoints(points []Trkpt) float64 {
	return points[len(points)-1].Time.Sub(points[0].Time).Seconds()
}

//Sums Duration of all Standings in seconds
func CalculateDurationOfStandings(standings *list.List) float64 {
	var duration float64 = 0
	for x := standings.Front(); x != nil; x = x.Next() {
		duration += x.Value.(Standing).End.Sub(x.Value.(Standing).Start).Seconds()
	}
	return duration
}

//Gets the Top Speed of multiple gps Points
//Input: multiple Gps Points
//Output: max speed in meters per second
func CalculateTopSpeed(points []Trkpt) float64 {
	point0 := points[0]
	var max float64 = 0
	for _, x := range points {
		speed := CalculateSpeedBetweenTwoGpxPoints(point0, x)
		if max < speed {
			max = speed
		}
		point0 = x
	}
	return max
}

//Executes a full analysis of a gpx file
//Input: Content of the Gpx file, comment, type of activity, filenames
//Output: Analysed activity
func AnalyseGpxObject(gpx Gpx, comment, activityType, fileName, jsonFileName string) AnalysedActivity {
	var points []Trkpt
	activity := activities.GetStateById(activityType)
	for _, x := range gpx.Tracking.Trkseg {
		points = append(points, x.Trkpt...)
	}
	if len(points) == 0 {
		return AnalysedActivity{
			Start:                    gpx.Metadata.Time,
			End:                      gpx.Metadata.Time,
			FullDistance:             0,
			MaxSpeed:                 0,
			AvgSpeed:                 0,
			AvgSpeedWithoutStanding:  0,
			Duration:                 0,
			DurationStandings:        0,
			DurationWithoutStandings: 0,
			Standings:                nil,
			Comment:                  comment,
			FileName:                 fileName,
			Activity:                 activity,
			PlausibleActivity:        false,
		}
	}
	duration := CalculateDurationOfAllGpxPoints(points)
	standings := FindStandings(points, activity)
	durationStandings := CalculateDurationOfStandings(standings)
	avgSpeedWithoutStandings := CalculateAvgSpeedWithoutStandings(points, standings)

	analysis := AnalysedActivity{
		Start:                    points[0].Time,
		End:                      points[len(points)-1].Time,
		FullDistance:             CalculateFullDistanceOfMultipleGpxPoints(points),
		MaxSpeed:                 CalculateTopSpeed(points),
		AvgSpeed:                 CalculateAvgSpeedBetweenAllGpxPoints(points),
		AvgSpeedWithoutStanding:  avgSpeedWithoutStandings,
		Duration:                 duration,
		DurationStandings:        durationStandings,
		DurationWithoutStandings: duration - durationStandings,
		Standings:                standings,
		Comment:                  comment,
		FileName:                 fileName,
		JsonFileName:             jsonFileName,
		Activity:                 activity,
		PlausibleActivity:        activity.IsPlausibleAvgSpeed(avgSpeedWithoutStandings),
	}

	return analysis
}
