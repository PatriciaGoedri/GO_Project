/*
created: 4688442
*/
package gpx

import (
	"../login"
	"encoding/json"
	"fmt"
	"html/template"
	"io/ioutil"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
)

//Handler for the index side
//Loads the activities for the user id and may select them by the search text
func IndexHandler(w http.ResponseWriter, r *http.Request, searchText string) {
	t, _ := template.ParseFiles(path.Join(".", "template", "index.html"))
	fmt.Println(w, "List activities")
	userName, _, _ := r.BasicAuth()
	userId := login.GetIdForUser(userName)

	analysedActivities := ReadAllActivities(w, userId)
	if searchText != "" {
		analysedActivities = SelectRelevantActivities(analysedActivities, searchText)
	}
	data := ConvertActivitiesToOutput(analysedActivities)

	t.Execute(w, data)
}

//Selects the activities by a search text
//Only returns activities where the comment contains the search text
//Not case sensitive
func SelectRelevantActivities(activities []AnalysedActivity, searchText string) []AnalysedActivity {
	var selected []AnalysedActivity
	searchText = strings.ToLower(searchText)

	for _, x := range activities {
		if strings.Contains(strings.ToLower(x.Comment), searchText) {
			selected = append(selected, x)
		}
	}

	return selected
}

//Converts the analysed activities to the output format
//All elements will be parsed as string and some are formatted
func ConvertActivitiesToOutput(activities []AnalysedActivity) []OutputActivity {
	var outputs []OutputActivity

	for _, x := range activities {
		outputs = append(outputs, OutputActivity{
			Activity:          x.Activity.String(),
			Start:             x.Start.Format("2006-01-02 15:04:05"),
			End:               x.End.Format("2006-01-02 15:04:05"),
			Duration:          strconv.FormatFloat(x.DurationWithoutStandings/60, 'f', 0, 32),
			DurationStandings: strconv.FormatFloat(x.DurationStandings/60, 'f', 0, 32),
			Distance:          strconv.FormatFloat(x.FullDistance/1000, 'f', 4, 32),
			AvgSpeed:          strconv.FormatFloat(x.AvgSpeedWithoutStanding*3.6, 'f', 2, 32),
			MaxSpeed:          strconv.FormatFloat(x.MaxSpeed*3.6, 'f', 2, 32),
			Comment:           x.Comment,
			PlausibleActivity: strconv.FormatBool(x.PlausibleActivity),
			FileName:          x.FileName,
			JsonFileName:      x.JsonFileName,
		})
	}

	return outputs
}

//Reads all activities of a user
//Checks the user directory and parse all json files in it
//Input: User id
//Output: all activities of the user
func ReadAllActivities(w http.ResponseWriter, userId string) []AnalysedActivity {
	var activities []AnalysedActivity

	files := GetFilesOfFolder(userId, w)
	if files == nil {
		return activities
	}

	for _, f := range files {
		if f.IsDir() {
			continue
		}
		done, activity := ReadJsonFile(w, filepath.Join(userId, f.Name()))
		if !done {
			continue
		}
		activities = append(activities, activity)
	}

	sort.Slice(activities, func(i, j int) bool {
		return activities[j].Start.Before(activities[i].Start)
	})

	return activities
}

//Reads and parse a Json file of a analysed activity
//Input: a path to a file
//Output: was the reading successfully and the object of the file
func ReadJsonFile(w http.ResponseWriter, fileName string) (bool, AnalysedActivity) {
	jsonFile, err := os.Open(fileName)
	if err != nil {
		fmt.Println(w, "Error reading files")
		return false, AnalysedActivity{}
	}

	byteValue, _ := ioutil.ReadAll(jsonFile)
	var activity AnalysedActivity
	json.Unmarshal(byteValue, &activity)
	jsonFile.Close()
	return true, activity
}

//Returns all files of a folder
func GetFilesOfFolder(userId string, w http.ResponseWriter) []os.FileInfo {
	if _, err := os.Stat(userId); os.IsNotExist(err) {
		fmt.Println(w, "No activities found")
		return nil
	}

	files, err := ioutil.ReadDir(userId)
	if err != nil {
		fmt.Println(w, "Error reading files")
		return nil
	}
	return files
}
