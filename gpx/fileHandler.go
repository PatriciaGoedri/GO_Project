/*
created: 4688442
*/
package gpx

import (
	"archive/zip"
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"strings"
)

//Unzips all GPX Files from a zip and save it in a user directory
//The zip file will be deleted after unzip
//Input: Path to the zip, userId to get the directory
//Output: List of unzipped GPX files and error
func Unzip(filePath, userId string) ([]string, error) {
	var fileNames []string
	gpxFolder := path.Join(userId, "gpx")

	r, err := zip.OpenReader(filePath)
	if err != nil {
		return fileNames, err
	}
	defer r.Close()
	for _, f := range r.File {
		if f.FileInfo().IsDir() {
			continue
		}

		fileExtension := strings.ToLower(f.Name[len(f.Name)-4:])
		if fileExtension != ".gpx" {
			continue
		}

		file, err := f.Open()
		if err != nil {
			return fileNames, err
		}

		extractFile, err := ioutil.TempFile(gpxFolder, "upload-*.gpx")
		if err != nil {
			return fileNames, err
		}

		fileByte, err := ioutil.ReadAll(file)
		if err != nil {
			return fileNames, err
		}
		extractFile.Write(fileByte)

		fileNames = append(fileNames, extractFile.Name())

		extractFile.Close()
		file.Close()
	}
	return fileNames, nil
}

//Deletes the given file and returns if it was successfully
func DeleteFile(fileName string) bool {
	err := os.Remove(fileName)
	if err != nil {
		fmt.Println(err)
		return false
	}
	return true
}

//Extracts the user id of a file name
func GetUserIdByFileName(fileName string) string {
	split := TryGetPathSplit(fileName)
	if split != "" {
		spaceLoc := strings.Index(fileName, split)
		if spaceLoc > 0 {
			runes := []rune(fileName)
			return string(runes[0:spaceLoc])
		}
	}
	return ""
}

//Tries to get the split char from the filename
//Could be different between Windows and Linux
func TryGetPathSplit(fileName string) string {
	if strings.Contains(fileName, "\\") {
		return "\\"
	} else if strings.Contains(fileName, "/") {
		return "/"
	}
	return ""
}
