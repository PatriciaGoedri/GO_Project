/*
created: 4688442
*/
package gpx

import (
	"fmt"
	"html/template"
	"net/http"
	"path"
)

//Handler to show the delete activity side
func DeleteActivity(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Show Delete Activity")
	r.ParseMultipartForm(1024 * 16)
	fileName := r.FormValue("fileName")
	jsonFile := r.FormValue("jsonFileName")
	data := struct {
		FileName, JsonFileName string
	}{
		fileName,
		jsonFile,
	}

	t, _ := template.ParseFiles(path.Join(".", "template", "deleteactivity.html"))
	t.Execute(w, data)
}

//Handler to delete a activity
//Activity will be deleted from the file system
func ExecuteDeleteActivity(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Delete Activity")
	r.ParseMultipartForm(1024 * 16)
	fileName := r.FormValue("fileName")
	jsonFile := r.FormValue("jsonFileName")

	done := DeleteFile(jsonFile)
	if !done {
		println(w, "Error delete activity")
		return
	}
	DeleteFile(fileName)
	IndexHandler(w, r, "")
}

//Handler to cancel the deletion of a activity
func CancelDeleteActivity(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Cancel Delete Activity")
	r.ParseMultipartForm(1024 * 16)
	IndexHandler(w, r, "")
}
