/*
created: 1100592 ; 4688442
*/
package main

import (
	"./gpx"
	l "./login"
	"html/template"
	"log"
	"net/http"
)

func main() {

	http.HandleFunc("/", login)
	//http.HandleFunc("/login", l.LoggingMiddleware(l.SecureHeaders(l.LoginUser)))
	http.HandleFunc("/registry.html", l.RegisterUser)
	http.HandleFunc("/index.html", l.LoggingMiddleware(l.SecureHeaders(index)))
	http.HandleFunc("/UploadGpx", l.LoggingMiddleware(l.SecureHeaders(gpx.UploadGpx)))
	http.HandleFunc("/SearchActivity", l.LoggingMiddleware(l.SecureHeaders(gpx.SearchHandler)))
	http.HandleFunc("/EditActivity", l.LoggingMiddleware(l.SecureHeaders(gpx.EditActivity)))
	http.HandleFunc("/ExecuteEditActivity", l.LoggingMiddleware(l.SecureHeaders(gpx.ExecuteEditActivity)))
	http.HandleFunc("/DeleteActivity", l.LoggingMiddleware(l.SecureHeaders(gpx.DeleteActivity)))
	http.HandleFunc("/ExecuteDeleteActivity", l.LoggingMiddleware(l.SecureHeaders(gpx.ExecuteDeleteActivity)))
	http.HandleFunc("/CancelDeleteActivity", l.LoggingMiddleware(l.SecureHeaders(gpx.CancelDeleteActivity)))
	http.HandleFunc("/DownloadActivity", l.LoggingMiddleware(l.SecureHeaders(gpx.DownloadActivity)))
	log.Fatalln(http.ListenAndServeTLS(":8080", "login/cert.pem", "login/key.pem", nil)) //HTTPS with OPENSSL certificate

}

func login(w http.ResponseWriter, r *http.Request) {
	l, _ := template.ParseFiles("template/login.html")
	l.Execute(w, nil)
}

func index(w http.ResponseWriter, r *http.Request) {
	gpx.IndexHandler(w, r, "")
}
