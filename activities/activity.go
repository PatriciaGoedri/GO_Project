/*
created: 1100592 ; 4688442
*/
package activities

import "time"

type STATE uint8

const (
	_ STATE = iota //Darstellen der Enumeration
	jogging
	biking
	walking
	hiking
	unknown
)

func (s STATE) String() string {
	if s == jogging {
		return "Jogging"
	} else if s == biking {
		return "Biking"
	} else if s == walking {
		return "Walking"
	} else if s == hiking {
		return "Hiking"
	} else {
		return "Unknown Activity"
	}
}

func (s STATE) IsPlausibleAvgSpeed(avgSpeed float64) bool {
	if avgSpeed <= 0 {
		return false
	}
	if s == jogging {
		return avgSpeed <= 5.5
	} else if s == biking {
		return avgSpeed >= 5
	} else if s == walking {
		return avgSpeed <= 2
	} else if s == hiking {
		return avgSpeed <= 2
	} else {
		return false
	}
}

func (s STATE) GetStandingFactor() time.Duration {
	if s == jogging {
		return 6
	} else if s == biking {
		return 3
	} else if s == walking {
		return 10
	} else if s == hiking {
		return 12
	} else {
		return 0
	}
}

func GetStateById(id string) STATE {
	if id == "0" {
		return jogging
	} else if id == "1" {
		return biking
	} else if id == "2" {
		return walking
	} else if id == "3" {
		return hiking
	} else {
		return unknown
	}
}

func (s STATE) GetId() int {
	if s == jogging {
		return 0
	} else if s == biking {
		return 1
	} else if s == walking {
		return 2
	} else if s == hiking {
		return 3
	} else {
		return 0
	}
}
